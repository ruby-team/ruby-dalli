require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.pattern = 'test/**/test_*.rb'
  t.warning = true
  t.verbose = true
end
